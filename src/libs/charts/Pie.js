export const DefaultColors = ['#fde23e', '#f16e23', '#57d9ff', '#937e88']
export const PieTypes = {
  BASE: 'BASE',
  PIE_CHART: 'PIE_CHART',
  PIE_CHART_MEMBER: 'PIE_CHART_MEMBER'
}
export class PieChart {
  constructor (options) {
    /**
     * options: pie chart config
     * canvas: draw target and html element
     * data: chart data.
     * memberBalance: member balance
     * pieTitle: pie title.
     * ctx: reference to the drawing context
     */
    this.options = options
    this.canvas = options.canvas
    this.canvas.width = options.width
    this.canvas.height = options.height
    this.data = options.data || []
    this.memberBalance = options.memberBalance
    this.pieTitle = options.pieTitle
    this.doughnutHoleSize = options.doughnutHoleSize || 0
    this.ctx = this.canvas.getContext('2d')
    this.colors = options.colors || DefaultColors
  }

  /**
   * x: the X coordinate of the circle center
   * y: the Y coordinate of the circle center
   * radius: the X coordinate of the line end point
   * startAngle: the start angle in radians where the portion of the circle starts
   * endAngle: the end angle in radians where the portion of the circle ends
   */
  drawArc (x, y, radius, startAngle, endAngle) {
    this.ctx.beginPath()
    this.ctx.arc(x, y, radius, startAngle, endAngle)
    this.ctx.stroke()
  }

  drawPieSlice (x, y, radius, startAngle, endAngle, colors) {
    let colorsLen = colors.length

    if (colorsLen === 1) {
      this.ctx.fillStyle = colors[0]
      this.ctx.shadowColor = '#999999'
      this.ctx.shadowOffsetX = 3
      this.ctx.shadowOffsetY = 3
      this.ctx.shadowBlur = 3
    } else {
      let bgc = this.ctx.createLinearGradient(0, 0, 0, 500)
      bgc.addColorStop(0, '#00DDDD')
      bgc.addColorStop(0.4, '#009FCC')
      bgc.addColorStop(0.6, '#00BBFF')
      bgc.addColorStop(0.8, '#007799')
      bgc.addColorStop(1, '#CCEEFF')
      this.ctx.fillStyle = bgc
    }

    this.ctx.beginPath()
    this.ctx.moveTo(x, y)
    this.ctx.arc(x, y, radius, startAngle, endAngle)
    this.ctx.closePath()
    this.ctx.fill()
  }

  draw (TYPE) {
    switch (TYPE) {
      case PieTypes.BASE:
        this.basePie()
        break
      case PieTypes.PIE_CHART:
        break
      case PieTypes.PIE_CHART_MEMBER:
        this.memberBalancePie()
        break
    }
  }

  basePie () {
    let totalValue = 0
    let colorIndex = 0

    for (let categ in this.data) {
      var val = this.data[categ]
      totalValue += val
    }

    let startAngle = 0

    for (let categ in this.data) {
      val = this.data[categ]
      let sliceAngle = 2 * Math.PI * val / totalValue
      let x = this.canvas.width / 2
      let y = this.canvas.height / 2
      let radius = Math.min(this.canvas.width / 2, this.canvas.height / 2)
      let endAngle = startAngle + sliceAngle
      let colors = this.colors[colorIndex % this.colors.length]
      this.drawPieSlice(x, y, radius, startAngle, endAngle, colors)
      startAngle += sliceAngle
      colorIndex++
    }

    this.drawDougnut()
  }

  memberBalancePie () {
    let totalValue = 0
    let colorIndex = 0

    for (let categ in this.data) {
      var val = this.data[categ]
      totalValue += val
    }

    let startAngle = 0

    for (let categ in this.data) {
      val = this.data[categ]
      let sliceAngle = 2 * Math.PI * val / totalValue
      let x = this.canvas.width / 2
      let y = this.canvas.height / 2
      let radius = Math.min(this.canvas.width / 2, this.canvas.height / 2)
      let endAngle = startAngle + sliceAngle
      let colors = this.colors[colorIndex % this.colors.length]
      this.drawPieSlice(x, y, radius, startAngle, endAngle, colors)
      startAngle += sliceAngle
      colorIndex++
    }
    this.drawDougnut()
    this.drawChartTitle()
    this.drawBalance(this.memberBalance)
  }

  drawBalance (balance) {
    if (balance) {
      let fontSize = this.fontSize(400, 50)
      this.ctx.font = `${fontSize}px Arial`
      this.ctx.fillStyle = '#000'
      this.ctx.textAlign = 'center'
      this.ctx.fillText(this.memberBalance, this.canvas.width / 2, this.canvas.height / 2)
      // // let memberBalance = this.memberBalance.split('.')
      // // let aaa = String(memberBalance[0])
      // // let randomNemberArray = this.randomNemberArray(integer)
      // setTimeout(() => {
      //   this.ctx.clearRect(this.canvas.width * 0.1, this.canvas.height * 0.4, this.canvas.width * 0.8, this.canvas.height * 0.15)
      //   this.drawDougnut()
      // }, 25)
      // setTimeout(() => {
      //   this.ctx.rect(this.canvas.width * 0.1, this.canvas.height * 0.4, this.canvas.width * 0.8, this.canvas.height * 0.15)
      //   this.ctx.fillText(new Date().valueOf(), this.canvas.width / 2, this.canvas.height / 2)
      //   // this.ctx.fillStyle = '#fff'
      //   // this.ctx.fillRect(this.canvas.width * 0.1, this.canvas.height * 0.4, this.canvas.width * 0.8, this.canvas.height * 0.15)
      //   // this.ctx.fillText(new Date().valueOf(), this.canvas.width / 2, this.canvas.height / 2)
      // }, 50)
    }
  }

  drawChartTitle () {
    if (this.pieTitle) {
      let fontSize = this.fontSize(400, 30)
      this.ctx.font = `${fontSize}px Arial`
      this.ctx.fillStyle = '#000'
      this.ctx.textAlign = 'center'
      this.ctx.fillText(this.pieTitle, this.canvas.width / 2, this.canvas.height * 0.7)
    }
  }

  drawDougnut () {
    if (this.options.doughnutHoleSize) {
      this.drawPieSlice(
        this.canvas.width / 2,
        this.canvas.height / 2,
        this.options.doughnutHoleSize * Math.min(this.canvas.width / 2, this.canvas.height / 2),
        0,
        2 * Math.PI,
        ['#fff']
      )
    }
  }

  fontSize (fontBase, fontSize) {
    let ratio = fontSize / fontBase
    return this.canvas.width * ratio
  }

  randomNemberArray (number) {
    let n = 0
    let str = ''
    for (let i = 0; i < 20; i++) {
      n = Math.floor(Math.random() * number) + 1
      if (str.indexOf(n) > 0) {
        i -= 1
        continue
      } else {
        str += `${n} `
      }
    }
    return str.split(' ').sort((a, b) => a - b)
  }
}
