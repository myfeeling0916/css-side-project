export const DefaultColors = ['#fde23e', '#f16e23', '#57d9ff', '#937e88']
export const BarTypes = {
  BASE: 'BASE',
  BAR_CHART: 'BAR_CHART',
  BAR_CHART_MEMBER: 'BAR_CHART_MEMBER'
}
export class Bar {
  constructor (options) {
    /**
     * options: pie chart config
     * canvas: draw target and html element
     * data: chart data.
     * memberBalance: member balance
     * pieTitle: pie title.
     * ctx: reference to the drawing context
     */
    this.options = options
    this.canvas = options.canvas
    this.canvas.width = options.width
    this.canvas.height = options.height
    this.data = options.data || []
    this.memberBalance = options.memberBalance
    this.pieTitle = options.pieTitle
    this.doughnutHoleSize = options.doughnutHoleSize || 0
    this.ctx = this.canvas.getContext('2d')
    this.colors = options.colors || DefaultColors
  }

  draw (TYPE) {
    switch (TYPE) {
      case BarTypes.BASE:
        break
      case BarTypes.BAR_CHART:
        break
      case BarTypes.BAR_CHART_MEMBER:
        break
    }
  }
}
