import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Calendar from '@/components/Calendar'
import GeneratePhoto from '@/components/GeneratePhoto'
import PieChart from '@/components/PieChart'
import BarChart from '@/components/BarChart'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'hello',
    component: HelloWorld
  },
  {
    path: '/calendar',
    name: 'calendar',
    component: Calendar
  },
  {
    path: '/generatePhoto',
    name: 'generatePhoto',
    component: GeneratePhoto
  },
  {
    path: '/pieChart',
    name: 'pieChart',
    component: PieChart
  },
  {
    path: '/barChart',
    name: 'barChart',
    component: BarChart
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
