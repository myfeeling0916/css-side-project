
module.exports = {
  css: {
    loaderOptions: {
      scss: {
        data: `@import "@/styles/index.scss";`
      }
    }
  }
}
